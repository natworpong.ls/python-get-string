from StringContent.StringContent import StringContent


def actions():

    def ask_for_input() -> int:
        try:
            return int(input(f'{actions.objects}\nIndex: '))
        except TypeError as error:
            print(error)
        except Exception as error:
            print(error)
        finally:
            return -1

    def create_object() -> StringContent:
        name, chars, lines = input("Name of object: "), \
                             int(input("Number of characters: ")), \
                             int(input("Number of lines: "))
        content_obj = StringContent(name, chars, lines)
        actions.objects.append(content_obj)
        return content_obj

    def inspect_object_total_lines() -> int:
        return actions.objects[ask_for_input()].lines()

    def inspect_object_content() -> str:
        return actions.objects[ask_for_input()].scontent()

    def inspect_object_first_to_spec() -> str:
        return actions.objects[ask_for_input()].lines_contents(int(input("Lines: ")))

    def inspect_object_spec_to_latest() -> str:
        return actions.objects[ask_for_input()].latest_lines_contents(int(input("Lines: ")))

    def exit_program():
        print("Sayonara!")
        system = __import__('sys')
        system.exit(1)

    actions.objects = []

    actions.list = {
        1: create_object,
        2: inspect_object_total_lines,
        3: inspect_object_content,
        4: inspect_object_first_to_spec,
        5: inspect_object_spec_to_latest,
        6: exit_program
    }


def do(selected):
    action = lambda: actions.list.get(selected, lambda: 'Invalid')(); print(action())


def menu():
    while(True):
        print(
            "Menu:\n"
            "1.Instantiate Object.\n"
            "2.Inspect object's total line.\n"
            "3.Inspect object's content.\n"
            "4.Inspect object's first to specific line.\n"
            "5.Inspect object's specific to latest line.\n"
            "6.Exit.\n"
        )
        action = int(input("Choice: "))
        try:
            if (action > 0 and action < 7):
                do(action)
            else:
                print("Invalid choice")
        except TypeError as error:
            print("Invalid choice")
        except Exception as error:
            print(error)
            system = __import__('sys'); system.exit(-1)


def start():
    if __name__ == '__main__':
        actions()
        menu()


start()
