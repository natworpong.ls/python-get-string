from StringContent.StringGenerator import StringGenerator

class StringContent(StringGenerator):
	""" Content Class """

	__slots__ = '__name'

	def __init__(self, name: str, chars: int, lines: int):
		""" Class Initializer """
		super(StringContent, self).__init__(chars, lines)
		self.__name = name

	def __str__(self) -> str:
		""" String representations of an object. """
		return f'{self.__name}'

	def __repr__(self) -> repr:
		""" String representations of an object. """
		return f'{self.__name}'

	def scontent(self) -> str:
		""" Return contents. """
		return self._string

	def contents(self) -> repr:
		""" Return verbose contents. """
		return repr(self._string)

	def lines(self) -> int:
		""" Return lines """
		return self._string.count('\n') + 1

	def lines_contents(self, lines: int) -> str:
		""" Return content from first line to specific line. """
		return '\n'.join(self._string.split('\n', lines)[:lines])

	def latest_lines_contents(self, lines: int) -> str:
		""" Return content from specific line to latest line. """
		return '\n'.join(self._string.rsplit('\n', lines)[len(self._string.rsplit('\n', lines))-lines:])
