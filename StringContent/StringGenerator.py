class StringGenerator:
	""" For testing purpose by PengPeng. """
	strm, rand = __import__('string'), __import__('random')

	__slots__ = '_string'

	def __init__(self, chars: int, lines: int):
		""" Class Initializer """
		self._string = self.generate_string(chars, lines)

	def __str__(self) -> str:
		""" String representations of an object. """
		return f'{self._string}'

	def __repr__(self) -> repr:
		""" String representations of an object. """
		return f'{self._string}'

	def generate_string(self, chars: int, lines: int) -> str:
		""" Generate random string from ascii_lowercase or ascii_uppercase. """
		return ''.join([''.join(StringGenerator.rand.choice(StringGenerator.rand.choice({
						0: StringGenerator.strm.ascii_lowercase,
						1: StringGenerator.strm.ascii_uppercase})) for i in range(chars)) +
						'\n' for j in range(lines)]).rstrip('\n')

